const { request, response } = require("express");
const mongoose = require("mongoose");
const courseModel = require("../model/courseModel");

const getAllCourse = (request,response) => {
    // B1: thu thập dữ liệu
    // B2: kiểm tra dữ liệu
    // B3: xử lý và hiển thị dữ liệu
    courseModel.find((error,data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            return response.status(200).json({
                message: "Successfully get all course",
                course : data
            })
        }
    })
}

const getCourseById = (request,response) => {
    // B1: thu thập dữ liệu
    let id = request.params.id
    // B2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is inValid"
        })
    }
    // B3: xử lý và hiển thị dữ liệu
    courseModel.findById(id,(error,data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error ${error.message}`
            })
        } else {
            return response.status(200).json({
                message: 'Successfully get Course by id',
                course: data
            })
        }
    })
}
const createCourse = (request,response) => {
    // B1: thu thập dữ liệu
    let body = request.body
    // B2: kiểm tra dữ liệu
    if(!body.courseCode) {
        return response.status(400).json({
            message: "courseCode is required"
        })
    };
    if(!body.courseName) {
        return response.status(400).json({
            message: "couseName is required"
        })
    };
    if(!body.price) {
        return response.status(400).json({
            message: "price is required"
        })
    };
    if(!Number.isInteger(body.price)) {
        return response.status(400).json({
            message: "price is invalid"
        })
    };
    if(!body.discountPrice) {
        return response.status(400).json({
            message: "discountPrice is required"
        })
    };
    if(!Number.isInteger(body.discountPrice)) {
        return response.status(400).json({
            message: "discountPrice is invalid"
        })
    };
    if(!body.duration) {
        return response.status(400).json({
            message: "duration is required"
        })
    };
    if(!body.level) {
        return response.status(400).json({
            message: "level is required"
        })
    };
    if(!body.coverImage) {
        return response.status(400).json({
            message: "coverImage is required"
        })
    };
    if(!body.teacherName) {
        return response.status(400).json({
            message: "teacherName is required"
        })
    };
    if(!body.teacherPhoto) {
        return response.status(400).json({
            message: "teacherPhoto is required"
        })
    };
    // B3: xử lý và hiển thị kết quả
    let newCourse = new courseModel({
        _id: mongoose.Types.ObjectId(),
        courseCode : body.courseCode,
        courseName : body.courseName,
        price : body.price,
        discountPrice : body.discountPrice,
        duration : body.duration,
        level : body.level,
        coverImage : body.coverImage,
        teacherName : body.teacherName,
        teacherPhoto : body.teacherPhoto,
        isPopular : body.isPopular,
        isTrending : body.isTrending,
    })
    courseModel.create(newCourse, (error, data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error ${error.message}`
            })
        } else {
            return response.status(201).json({
                message: "Successfully created course",
                course: data
            })
        }
    })
}

const updateCourse = (request,response) => {
    // B1: thu thập dữ liệu
    let id = request.params.id;
    let body = request.body;
    // B2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid"
        })
    }
    if(!body.courseCode) {
        return response.status(400).json({
            message: "courseCode is required"
        })
    };
    if(!body.courseName) {
        return response.status(400).json({
            message: "couseName is required"
        })
    };
    if(!body.price) {
        return response.status(400).json({
            message: "price is required"
        })
    };
    if(!Number.isInteger(body.price)) {
        return response.status(400).json({
            message: "price is invalid"
        })
    };
    if(!body.discountPrice) {
        return response.status(400).json({
            message: "discountPrice is required"
        })
    };
    if(!Number.isInteger(body.discountPrice)) {
        return response.status(400).json({
            message: "discountPrice is invalid"
        })
    };
    if(!body.duration) {
        return response.status(400).json({
            message: "duration is required"
        })
    };
    if(!body.level) {
        return response.status(400).json({
            message: "level is required"
        })
    };
    if(!body.coverImage) {
        return response.status(400).json({
            message: "coverImage is required"
        })
    };
    if(!body.teacherName) {
        return response.status(400).json({
            message: "teacherName is required"
        })
    };
    if(!body.teacherPhoto) {
        return response.status(400).json({
            message: "teacherPhoto is required"
        })
    };
    // B3: xử lý và hiển thị dữ liệu
    courseModel.findByIdAndUpdate(id,body,(error,data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error ${error.message}`
            })
        } else {
            return response.status(200).json({
                message: "Successfully update course",
                course : data
            })
        }
    })
}

const deleteCourse = (request,response) => {
    // B1: thu thập dữ liệu
    let id = request.params.id;
    // B2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message: "id is invalid"
        })
    }
    // B3: xử lý và hiển thị kết quả
    courseModel.findByIdAndDelete(id,(error,data) => {
        if(error) {
            return response.status(500).json({
                message: `Internal server error ${error.message}`
            })
        } else {
            return response.status(204).json({
                message: "Successfully delete course",
                course: data
            })
        }
    })
}
module.exports = { createCourse, getAllCourse, getCourseById, updateCourse, deleteCourse }
