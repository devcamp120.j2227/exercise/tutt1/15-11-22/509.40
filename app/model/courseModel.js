const mongoose = require('mongoose');
const CourseDB = mongoose.Schema;
const courseSchema = new CourseDB({
    _id: mongoose.Types.ObjectId,
    courseCode: {
        type: String,
        unique: true,
        require: true
    },
	courseName: {
        type: String,
        require: true
    },
	price: {
        type: Number,
        require: true
    },
	discountPrice: {
        type: Number,
        require: true
    },
	duration: {
        type: String,
        require: true
    },
	level: {
        type: String,
        require: true
    },
	coverImage: {
        type: String,
        require: true
    },
    teacherName: {
        type: String,
        require: true
    },
	teacherPhoto: {
        type: String,
        require: true
    },
	isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false
    }
},
{
    timestamps: {
        createdAt: "ngayTao",
        updatedAt: "ngayCapNhat"
    }
}
)

module.exports = mongoose.model("course",courseSchema);
