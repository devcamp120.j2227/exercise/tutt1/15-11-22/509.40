const express = require("express");
const { createCourse, getAllCourse, getCourseById, updateCourse, deleteCourse } = require("../controllers/courseControllers");

const router = express.Router();

router.get("/courses", getAllCourse);
router.get("/courses/:id", getCourseById);
router.post("/courses", createCourse);
router.put("/courses/:id", updateCourse); 
router.delete("/courses/:id", deleteCourse);

module.exports = router;
