const express = require("express");
const app = express();
app.use(express.json());

const mongoose = require("mongoose");
const port = 8000;
// Kết nối với mongoose
mongoose.connect("mongodb://localhost:27017/{course}", (error) => {
    if(error) throw error;
    console.log("Successfully connected");
})
const path = require("path");
 
app.get("/courses365",(req,res) => {
    res.sendFile(path.join(__dirname + "/views/project.course365.v10/index.html"));
})

// import router
const courseRouter = require("./app/router/courseRouter");
// import model
//const courseModel = require("./app/model/courseModel");

app.use(courseRouter)
//app.use(courseModel)
app.use(express.static(__dirname + "/views/project.course365.v10"));
app.listen(port, () => {
    console.log("App listening on port", port);
})
