var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isRecommended: true,
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isRecommended: true,
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isRecommended: true,
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isRecommended: true,
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isRecommended: false,
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isRecommended: false,
            isPopular: true,
            isTrending: false
        }
    ]
}
function onPageLoading() {
    filterRecommended();
    filterMostPopular();
    filterTrending();
}
// Hàm lọc dữ liệu Recommended và hiển thị ra html
function filterRecommended() {
    var vDataRecommended = gCoursesDB.courses.filter((item) =>
        item.isRecommended == true
    );
    var vRecommended = "";
    for(var bI = 0; bI < vDataRecommended.length; bI++) {
        vRecommended = vRecommended + `<div class="col-sm-3 " >
        <div class="card" style="width: 15rem;">
          <img src = ${vDataRecommended[bI].coverImage} class="card-img-top" alt="...">
          <div class="card-body">
            <a class="card-title">${vDataRecommended[bI].courseName}</a>
            <p class="card-text"><i class="far fa-clock"></i> ${vDataRecommended[bI].duration}  ${vDataRecommended[bI].level}</p>
            <p class="card-text"> $ ${vDataRecommended[bI].discountPrice} <del class="text-secondary"> $${vDataRecommended[bI].price}</del> </p>
          </div>
          <div class="card-footer bg-transparent">
            <p  class="navbar-brand form-group" href="#" style="font-size:medium" > 
              <img src = ${vDataRecommended[bI].teacherPhoto} alt="logo" style="width:40px; border-radius:50%;"> 
              ${vDataRecommended[bI].teacherName}
            </p>
            <i class="far fa-bookmark"></i>
          </div>
        </div>
      </div>`
    }
    $(".recommended").html(vRecommended);
}
// Hàm lọc dữ liệu Most Popular và hiển thị ra html
function filterMostPopular(){
    var vDataMostPopular = gCoursesDB.courses.filter((item)=>
         item.isPopular == true
    )
    var vMostPopular = "";
    for(var bI =0; bI < vDataMostPopular.length; bI++){
        vMostPopular = vMostPopular + `<div class="col-sm-3 " >
        <div class="card" style="width: 15rem;">
          <img src = ${vDataMostPopular[bI].coverImage} class="card-img-top" alt="...">
          <div class="card-body">
            <a class="card-title">${vDataMostPopular[bI].courseName}</a>
            <p class="card-text"><i class="far fa-clock"></i> ${vDataMostPopular[bI].duration}  ${vDataMostPopular[bI].level}</p>
            <p class="card-text"> $ ${vDataMostPopular[bI].discountPrice} <del class="text-secondary"> $${vDataMostPopular[bI].price}</del> </p>
          </div>
          <div class="card-footer bg-transparent">
            <p  class="navbar-brand form-group" href="#" style="font-size:medium" > 
              <img src = ${vDataMostPopular[bI].teacherPhoto} alt="logo" style="width:40px; border-radius:50%;"> 
              ${vDataMostPopular[bI].teacherName}
            </p>
            <i class="far fa-bookmark"></i>
          </div>
        </div>
      </div>`
    }
    $(".most-popular").html(vMostPopular);
}
// Hàm lọc dữ liệu Trending và hiển thị ra html
function filterTrending(){
    var vDataTrending = gCoursesDB.courses.filter((item)=>
         item.isTrending == true
    )
    var vTrending = "";
    for(var bI =0; bI < vDataTrending.length; bI++){
        vTrending = vTrending + `<div class="col-sm-3 " >
        <div class="card" style="width: 15rem;">
          <img src = ${vDataTrending[bI].coverImage} class="card-img-top" alt="...">
          <div class="card-body">
            <a class="card-title">${vDataTrending[bI].courseName}</a>
            <p class="card-text"><i class="far fa-clock"></i> ${vDataTrending[bI].duration}  ${vDataTrending[bI].level}</p>
            <p class="card-text"> $ ${vDataTrending[bI].discountPrice} <del class="text-secondary"> $${vDataTrending[bI].price}</del> </p>
          </div>
          <div class="card-footer bg-transparent">
            <p  class="navbar-brand form-group" href="#" style="font-size:medium" > 
              <img src = ${vDataTrending[bI].teacherPhoto} alt="logo" style="width:40px; border-radius:50%;"> 
              ${vDataTrending[bI].teacherName}
            </p>
            <i class="far fa-bookmark"></i>
          </div>
        </div>
      </div>`
    }
    $(".trending").html(vTrending);
}
